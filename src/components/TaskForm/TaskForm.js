import './TaskForm.css';
import React, { Component } from 'react';
import TaskDetail from '../TaskDetail/TaskDetail';
import { statuses } from '../../App';

class TaskForm extends Component {

    constructor(props) {
        super(props);
        this.state = { task: props.task };
    }

    render() {
        return (
            <>
                {this.props.type === 'edit' && <h2>{this.props.task.task} ({statuses[this.props.task.status].label})</h2>}
                {this.props.type === 'create' && <h2>New Task ({statuses.todo.label})</h2>}
                <div>
                    {this.props.type === 'edit' && <TaskDetail detail={this.props.task.detail} />}
                    <fieldset>
                        <legend>{this.props.type === 'edit' ? 'Edit' : 'Create'} Task</legend>
                        <form>
                            <div className='form-container'>
                                <div className='item remaining-days-field'>
                                    <div className='label'>Remaining Days</div>
                                    <div className='field'>
                                        <input type='text' value={this.state.task.remainingDays} onChange={(e) => this.handleChange(e, 'remainingDays')} />
                                    </div>
                                </div>
                                <div className='item task-field'>
                                    <div className='label'>Task Name</div>
                                    <div className='field'>
                                        <input type='text' value={this.state.task.task} onChange={(e) => this.handleChange(e, 'task')} />
                                    </div>
                                </div>
                                <div className='item created-field'>
                                    <div className='label'>Created Date</div>
                                    <div className='field'>
                                        <input type='text' value={this.state.task.detail.created} onChange={(e) => this.handleChange(e, 'created')} />
                                    </div>
                                </div>
                                <div className='item expire-field'>
                                    <div className='label'>Expire Date</div>
                                    <div className='field'>
                                        <input type='text' value={this.state.task.detail.expire} onChange={(e) => this.handleChange(e, 'expire')} />
                                    </div>
                                </div>
                            </div>
                            { this.props.type === 'create' && <button type='button' onClick={() => this.props.cbCreateEdit('create', this.props.task)}>Save</button>}
                        </form>
                    </fieldset>
                </div>
            </>
        );
    }

    handleChange(event, field) {
        console.log(event.target.value)
        if (field === 'created' || field === 'expire') {
            this.props.task.detail[field] = event.target.value;
        } else {
            this.props.task[field] = event.target.value;
        }
        this.setState({ task: this.props.task });
    }

}

export default TaskForm;