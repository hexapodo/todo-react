import './TaskDetail.css';
import React, { Component } from 'react';

class TaskDetail extends Component {

    render() {
        return (
            <div className={`task-detail ${this.props.detail.active ? "task-detail-100" : ""} ${!this.props.detail.active ? "task-detail-0" : ""} `}>
                <h3>Task Detail:</h3>
                <div><span className='title'>Creation date: </span>{this.props.detail.created}</div>
                <div><span className='title'>Expire on: </span>{this.props.detail.expire}</div>
                {
                    this.props.detail.anotations.map((anotation, index) =>
                        {
                            return <div key={index}>
                                <p>{anotation}</p>
                                <hr />
                            </div>
                        }
                    )
                }
            </div>
        );
    }
}

export default TaskDetail;