import './Task.css';
import React, { Component } from 'react';
import TaskDetail from '../TaskDetail/TaskDetail';
import { statuses } from '../../App';

class Task extends Component {

    render() {
        console.log(this.props);
        return (
            this.props.data.map((task, index) => {
                let arrow = task.detail.active ? '🔺' : '🔻';
                return <div className='container' key={index}>
                    <div className={'task task-' + task.status}>
                        <div className='arrow' onClick={() => this.onToggleArrow(task)}>
                            <span>{arrow}</span>
                        </div>
                        <div className='number'>{task.number}.</div>
                        <div className='task-container'>
                            <div className='task-name'>
                                {task.task}
                            </div>
                            {task.status !== 'completed' && <div className='remaining-days'>{task.remainingDays}</div>}
                            <div className='status'>{statuses[task.status].label}</div>
                            <div className='actions'>
                                <div className='action action-primary' onClick={() => { this.props.edit('comment', task) }}>Comment</div>
                                <div className='action action-primary' onClick={() => { this.props.edit('modifyStatus', task) }}>Status</div>
                                <div className='action action-primary' onClick={() => { this.props.edit('edit', task) }}>Edit</div>
                                <div className='action action-danger' onClick={() => { this.props.edit('remove', task) }}>Remove</div>
                            </div>
                        </div>
                    </div>
                    <TaskDetail className="task-detail-0" detail={task.detail} />
                </div>
            })

        );
    }

    onToggleArrow = (task) => {
        const newData = [...this.props.data];
        this.setState(newData.map((taskData) => {
            if (taskData.task === task.task) {
                taskData.detail.active = !taskData.detail.active
            }
            return taskData;
        }));
    }

}

export default Task;