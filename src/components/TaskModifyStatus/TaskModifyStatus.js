import "./TaskModifyStatus.css";
import React, { Component } from "react";
import TaskDetail from "../TaskDetail/TaskDetail";
import { statuses } from "../../App";

class TaskModifyStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: props.task.status,
        };
    }

    handleClick = (newStatus) => {
        this.props.task.status = newStatus;
        this.setState({ status: newStatus });
    };

    render() {
        return (
            <>
                <h2>
                    {this.props.task.task} (
                    {statuses[this.props.task.status].label})
                </h2>
                <div className={`task-modify-status`}>
                    <TaskDetail detail={this.props.task.detail} />
                    <fieldset>
                        <legend>Task Status</legend>
                        Status:
                        <br />
                        <input
                            type="radio"
                            id={statuses.todo.code}
                            defaultChecked={
                                this.state.status === statuses.todo.code
                            }
                            onClick={() => this.handleClick(statuses.todo.code)}
                            name="status"
                            value={statuses.todo.code}
                        />{" "}
                        <label htmlFor={statuses.todo.code}>
                            {statuses.todo.label}
                        </label>
                        <br />
                        <input
                            type="radio"
                            id={statuses.progress.code}
                            defaultChecked={
                                this.state.status === statuses.progress.code
                            }
                            onClick={() =>
                                this.handleClick(statuses.progress.code)
                            }
                            name="status"
                            value={statuses.progress.code}
                        />{" "}
                        <label htmlFor={statuses.progress.code}>
                            {" "}
                            {statuses.progress.label}{" "}
                        </label>
                        <br />
                        <input
                            type="radio"
                            id={statuses.completed.code}
                            defaultChecked={
                                this.state.status === statuses.completed.code
                            }
                            onClick={() =>
                                this.handleClick(statuses.completed.code)
                            }
                            name="status"
                            value={statuses.completed.code}
                        />{" "}
                        <label htmlFor={statuses.completed.code}>
                            {" "}
                            {statuses.completed.label}{" "}
                        </label>
                        <br />
                        <input
                            type="radio"
                            id={statuses.expired.code}
                            defaultChecked={
                                this.state.status === statuses.expired.code
                            }
                            onClick={() =>
                                this.handleClick(statuses.expired.code)
                            }
                            name="status"
                            value={statuses.expired.code}
                        />{" "}
                        <label htmlFor={statuses.expired.code}>
                            {" "}
                            {statuses.expired.label}{" "}
                        </label>
                        <br />
                        <input
                            type="radio"
                            id={statuses.alert.code}
                            defaultChecked={
                                this.state.status === statuses.alert.code
                            }
                            onClick={() =>
                                this.handleClick(statuses.alert.code)
                            }
                            name="status"
                            value={statuses.alert.code}
                        />{" "}
                        <label htmlFor={statuses.alert.code}>
                            {" "}
                            {statuses.alert.label}{" "}
                        </label>
                        <br />
                    </fieldset>
                </div>
            </>
        );
    }
}

export default TaskModifyStatus;
