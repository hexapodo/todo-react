import './TaskRemove.css';
import React, { Component } from 'react';
import TaskDetail from '../TaskDetail/TaskDetail';
import { statuses } from '../../App';

class TaskRemove extends Component {

    render() {
        return (
            <>
                <h2>{this.props.task.task} ({statuses[this.props.task.status].label})</h2>
                <div className={`task-remove`}>
                    <TaskDetail detail={this.props.task.detail} />
                    <fieldset>
                        <legend>Remove Task</legend>
                        Do you want to remove this task?<br />
                        <button className='action action-primary' onClick={() => this.props.cbRemove('no')}>No</button>
                        <button className='action action-danger' onClick={() => this.props.cbRemove('yes', this.props.task)}>Yes</button>
                    </fieldset>
                </div>
            </>
        );
    }

}

export default TaskRemove;