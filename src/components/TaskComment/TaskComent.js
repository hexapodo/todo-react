import './TaskComment.css';
import React, { Component } from 'react';
import { statuses } from '../../App';
import TaskDetail from '../TaskDetail/TaskDetail';

class TaskComment extends Component {

    render() {
        return (
            <>
                <h2>{this.props.task.task} ({statuses[this.props.task.status].label})</h2>
                <div className={`task-remove`}>
                    <TaskDetail detail={this.props.task.detail} />
                    <fieldset>
                        <legend>Task Comments</legend>
                        
                    </fieldset>
                </div>
            </>
            // <div className={`task-comment`}>
            //     <h2>Task comment</h2>
            // </div>
        );
    }
}

export default TaskComment;