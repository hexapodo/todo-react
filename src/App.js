import './App.css';
import React, { Component } from 'react';
// import Product from './components/Product/Product';
import Task from './components/Task/Task';
import TaskForm from './components/TaskForm/TaskForm';
import TaskRemove from './components/TaskRemove/TaskRemove';
import TaskComment from './components/TaskComment/TaskComent';
import TaskModifyStatus from './components/TaskModifyStatus/TaskModifyStatus';

const data = [
  {
    task: 'tarea 1',
    status: 'expired',
    remainingDays: 4,
    number: 1,
    detail: {
      active: false,
      anotations: [
        'anotation 1',
        'anotation 2',
        'anotation 3',
        'anotation 4',
        'anotation 5',
      ],
      created: '2022-03-06',
      expire: '2022-03-10',
    }
  },
  {
    task: 'tarea 2',
    status: 'todo',
    remainingDays: 4,
    number: 2,
    detail: {
      active: false,
      anotations: [
        'anotation 2',
      ],
      created: '2022-03-06',
      expire: '2022-03-10',
    }
  },
  {
    task: 'tarea 3',
    status: 'alert',
    remainingDays: 4,
    number: 3,
    detail: {
      active: false,
      anotations: [
        'anotation 3'
      ],
      created: '2022-03-06',
      expire: '2022-03-10',
    }
  },
  {
    task: 'tarea 4',
    status: 'completed',
    remainingDays: 4,
    number: 4,
    detail: {
      active: false,
      anotations: [
        'anotation 4',
      ],
      created: '2022-03-06',
      expire: '2022-03-10',
    }
  },
  {
    task: 'tarea 5',
    status: 'progress',
    remainingDays: 6,
    number: 5,
    detail: {
      active: false,
      anotations: [
        'anotation 5',
      ],
      created: '2022-03-06',
      expire: '2022-03-10',
    }
  }
];

export const statuses = {
  'todo': {
    code: 'todo',
    label: 'To do',
  },
  'progress': {
    code: 'progress',
    label: 'In Progress',
  },
  'completed': {
    code: 'completed',
    label: 'Completed',
  },
  'expired': {
    code: 'expired',
    label: 'Expired',
  },
  'alert': {
    code: 'alert',
    label: 'Alert',
  }
}

class App extends Component {

  state = {
    data: data,
    activeTask: null,
    edit: false,
    create: false,
    remove: false,
    comment: false,
    modifyStatus: false
  }
  render() {
    return (
      <div className="App">
        <h1>TODO LIST</h1>

        {!(this.state.edit || this.state.create || this.state.remove || this.state.comment || this.state.modifyStatus) &&
          <>
            <button className='action action-primary' onClick={() => this.handleClick('create')}>Add</button>
            <Task data={this.state.data} edit={this.handleClick} />
          </>
        }

        {(this.state.edit || this.state.create || this.state.remove || this.state.comment || this.state.modifyStatus) &&
          <button className='action action-primary' onClick={() => this.handleClick('back')}>Back</button>
        }

        {(this.state.edit || this.state.create) &&
          <TaskForm type={this.state.edit ? 'edit' : 'create'} cbCreateEdit={(action, task) => this.handleCreateEdit(action, task)} task={this.state.activeTask} />
        }

        {this.state.remove &&
          <TaskRemove task={this.state.activeTask} cbRemove={(action, task) => this.handleRemove(action, task)} />
        }

        {this.state.comment &&
          <TaskComment task={this.state.activeTask} cbComments={(action, task) => this.handleComments(action, task)} />
        }

        {this.state.modifyStatus &&
          <TaskModifyStatus task={this.state.activeTask} />
        }
      </div>
    );
  }

  handleClick = (action, task) => {
    this.closeAll();
    switch (action) {
      case 'create':
        const newTask = {
          remainingDays: '',
          task: '',
          status: 'todo',
          number: this.state.data.length + 1,
          detail: {
            active: false,
            anotations: [],
            created: '',
            expire: ''
          }
        }
        this.setState({
          create: true,
          activeTask: newTask
        });
        break;
      case 'edit':
        task.detail.active = true;
        this.setState({
          edit: true,
          activeTask: task
        });
        break;
      case 'remove':
        task.detail.active = true;
        this.setState({
          remove: true,
          activeTask: task
        });
        break;
      case 'comment':
        task.detail.active = true;
        this.setState({
          comment: true,
          activeTask: task
        });
        break;
      case 'modifyStatus':
        task.detail.active = true;
        this.setState({
          modifyStatus: true,
          activeTask: task
        });
        break;
      case 'back':
        this.closeAll()
        break;

      default:
        break;
    }
  }

  closeAll = () => {
    const originalData = [...this.state.data];
    originalData.forEach((data) => { data.detail.active = false });
    this.setState({
      edit: false,
      create: false,
      remove: false,
      comment: false,
      modifyStatus: false
    })
  }

  handleRemove = (action, task) => {
    switch (action) {
      case 'no':
        this.closeAll();
        break;
      case 'yes':
        const currentData = [...this.state.data];
        currentData.splice(currentData.indexOf(task), 1);
        this.setState({
          data: currentData
        });
        this.closeAll();
        break;

      default:
        break;
    }
  }

  handleCreateEdit = (action, task) => {
    switch (action) {
      case 'edit':
        this.closeAll();
        break;
      case 'create':
        const newData = [...this.state.data];
        newData.push(this.state.activeTask);
        console.log('newData', newData);
        this.setState({
          data: newData
        });
        this.closeAll();
        break;
      default:
        break;
    }
    console.log(action, task)
  }

  handleComments = (action, task) => {
    switch (action) {
      default:
        break;
    }
    console.log(action, task)
  }
}

export default App;
